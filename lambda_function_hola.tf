module "lambda_hola" {
  source  = "terraform-aws-modules/lambda/aws"
  version = "~> 1.0"

  function_name = "${random_pet.this.id}-lambda-hola"
  description   = "CI-CD Test"
  handler       = "lambda.handler"
  runtime       = "python3.7"
  publish       = true

  source_path = "hola/src/"

  lambda_role          = "arn:aws:iam::166333995750:role/lambda-role"
  create_role = false

  timeout       = "10"
  
  attach_tracing_policy    = true
  
  // create_current_version_allowed_triggers = false
  
  # allowed_triggers = {
  #   AllowExecutionFromAPIGateway = {
  #     service    = "apigateway"
  #     arn     = module.api_gateway.this_apigatewayv2_api_execution_arn
  #   }
  # } 
}

module "lambda_alias_hola" {
  source  = "terraform-aws-modules/lambda/aws//modules/alias"
  version = "~> 1.0"

  name = "dev"

  function_name = module.lambda_hola.this_lambda_function_name

  # Set function_version when creating alias to be able to deploy using it,
  # because AWS CodeDeploy doesn't understand $LATEST as CurrentVersion.
  function_version = module.lambda_hola.this_lambda_function_version
}
