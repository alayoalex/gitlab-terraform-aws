# #################
# Lambda Function
# #################

output "lambda_hola" {
  description = "The ARN of the Lambda Function"
  value       = module.lambda_hola.this_lambda_function_arn
}

output "lambda_hola_invoke_arn" {
  description = "The Invoke ARN of the Lambda Function"
  value       = module.lambda_hola.this_lambda_function_invoke_arn
}